from io import StringIO
import pandas as pd
import os, sqlite3

#------------------------------------------------------------------------------
def convert(path: str, format: str | list, split: int = 1, labels: dict = None, filter: str = None) -> list:
    """ Convertisseur qui permet de passer un fichier d’un format à un autre
        Format pris en charge: ["json", "xml", "csv", "xlsx", "db"] -> .db à terminer
    Args:
        path (str): chemnin vers le fichier source
        format (str | list): format du fichier de sortie "json", "xml", "csv", "xlsx", "sql"
        split (int): découpe les données en différents fichiers de même taille suivants les LIGNES
        labels (dict): permet de changer les noms de colonnes
        filter (str): permet de trier le document avant export
    Returns:
        list: retourne la liste des chemins
    """
    #================================================================
    def file_statut(path: str):
        file = os.path.basename(path)
        file_name, ext = os.path.splitext(file)
        metadata = {"path": path, 
                        "directory_name": os.path.dirname(path), 
                        "file": file, 
                        "file_name": file_name, 
                        "extension": ext}
        return metadata
    #================================================================
    def isistance_format(format_to_chech: str | list):
        if isinstance(format_to_chech, str):
            format: list = [format_to_chech]
        else:
            format: list = format_to_chech
        return format
    #================================================================
    def change_labels(dataframe, labels_dict: dict):
        dataframe.rename(columns=labels_dict, inplace=True)
    #================================================================
    def split_dataframe(dataframe, nb_split: int):
        # Spéparation du dataframe en plusieurs dataframe
        total_row = dataframe.shape[0]
        nb_lign_by_split = round(total_row / nb_split)
        nb_lign_for_last_part = total_row - (nb_lign_by_split * nb_split)
        df_list = []
        for n in range(1, nb_split):
            # borne pour index: n° ligne
            if n == 1:
                start_part = 0
            else:
                start_part = (n - 1) * nb_lign_by_split
            end_part = (n * nb_lign_by_split)
            # Découpe du dataframe
            df_split_n = dataframe.iloc[start_part : end_part,:]
            # Sauvegarde du df dans une liste
            df_list.append(df_split_n)
        # borne pour index: n° ligne
        if nb_lign_for_last_part == 0:
            start_last_part = (nb_split - 1) * nb_lign_by_split 
            end_last_part = nb_split * nb_lign_by_split 
        else:
            start_last_part = (nb_split - 1) * nb_lign_by_split 
            end_last_part = nb_split * nb_lign_by_split + nb_lign_for_last_part
        # Découpe du dataframe
        df_split_last = dataframe.iloc[start_last_part : end_last_part,:]
        # Sauvegarde du df dans une liste
        if not df_split_last.empty:
            df_list.append(df_split_last)
        return df_list
    #================================================================
    def read_file(metadata: dict, connexion):
        path_for_read = metadata["directory_name"]  + "/" + metadata["file_name"]
        format_type = metadata["extension"].replace('.', '')
        if format_type == "json": # json fonctionnel
            return pd.read_json(path_for_read + '.json', orient='columns')
        if format_type == "xml": # xml fonctionnel
            with open(path_for_read + '.xml') as obj:
                xml = obj.read()
            df_xml = pd.read_xml(StringIO(xml))
            df_xml.set_index(df_xml.columns[0], inplace=True)
            return df_xml
        if format_type == "csv": # csv fonctionnel
            return pd.read_csv(path_for_read + '.csv', index_col=0)
        if format_type == "xlsx": # xlsx fonctionnel
            return pd.read_excel(path_for_read + '.xlsx', index_col=0)
        if format_type == "sql":
            print("Format non pris en charge: code à terminé")
            try:
                return pd.read_sql(f'SELECT * FROM {metadata["file_name"]}', connexion)
            except sqlite3.Error as error:
                print("Error while connecting to sqlite", error)
    #================================================================
    def write_file(dataframe, metadata: dict, extension, connexion, part: str = None):
        if part != None:
            path_of_write_file = metadata["directory_name"]  + "/" + metadata["file_name"] + part
        else:
            path_of_write_file = metadata["directory_name"]  + "/" + metadata["file_name"]
        if extension == "json": # json fonctionnel
            path_of_write_file = path_of_write_file + '.json'
            dataframe.to_json(path_of_write_file , orient='columns')
            return path_of_write_file
        if extension == "xml": # xml fonctionnel
            path_of_write_file = path_of_write_file + '.xml'
            xml = dataframe.to_xml()
            with open(path_of_write_file , "w") as file:
                file.write(xml)
            return path_of_write_file
        if extension == "csv": # csv fonctionnel
            path_of_write_file = path_of_write_file + '.csv'
            dataframe.to_csv(path_of_write_file)
            return path_of_write_file
        if extension == "xlsx": # excel fonctionnel
            path_of_write_file = path_of_write_file + '.xlsx'
            dataframe.to_excel(path_of_write_file)
            return path_of_write_file
        if extension == "sql":
            print("Format non pris en charge: code à terminé")
            path_of_write_file = path_of_write_file + '.db'
            try:
                dataframe.to_sql(name=path_of_write_file , con=connexion)
            except sqlite3.Error as error:
                print("Error while connecting to sqlite", error)
            return path_of_write_file
    #================================================================
    def write_multiple(list_dataframe, format_to_out: list, metadata: dict, connexion):
        list_of_path = []
        for index, dataframe in enumerate(list_dataframe):
            if len(list_dataframe) > 1:
                part_of_df = f"_part_{index + 1}"
            else:
                part_of_df = None
            for ext in format_to_out:
                if ext != metadata["extension"].replace('.', ''):
                    # print("-->", metadata["extension"].replace('.', ''), "to", ext)
                    path_of_multiple_write_file = write_file(dataframe, metadata, ext, connexion, part_of_df)
                    list_of_path.append(path_of_multiple_write_file)
        return list_of_path
    #================================================================
    # Récupération des métadata sur fichiers
    meta_file = file_statut(path)
    print("Métadonnée sur le fichier:\n", meta_file)
    #____________________________________________
    # Création de la connexion SQL
    # print("-" * 25, "Connection SQLite:", "-" * 25)
    database = meta_file["directory_name"] + "/" + meta_file["file_name"] + '.db'
    sqliteConnection = sqlite3.connect(database)
    # print("Database created and Successfully Connected to SQLite")
    # print("Connexion SQL:", sqliteConnection)
    #____________________________________________
    # Vérification du format
    is_format = isistance_format(format)
    # print("Vérifier format:", is_format)
    #____________________________________________
    # Lecture sur fichier et transformation en dataframe
    df = read_file(meta_file, sqliteConnection)
    # print("Dataframe read:\n", df)
    #____________________________________________
    # Changement des noms de colonnes pour le fichier de sortie
    if labels != None:
        change_labels(df, labels)
    #____________________________________________
    # Découpage des données en différents fichiers de même taille suivants les LIGNES
    list_of_df: list = []
    if split != 1:
        list_of_df = split_dataframe(df, split)
        # print("cond != 1: split", list_of_df)
    else:
        list_of_df.append(df)
        # print("cond autre: split", list_of_df)
    #____________________________________________
    # Ecriture du dataframe sous différents format
    # print("Ecriture des fichiers:")
    path_of_file = write_multiple(list_of_df, is_format, meta_file, sqliteConnection)
    #____________________________________________
    if sqliteConnection:
        # print("The SQLite connection is closed")
        sqliteConnection.close()
    return path_of_file
#------------------------------------------------------------------------------




