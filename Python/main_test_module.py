import pandas as pd
import numpy as np
import time

from module import convert

#------------------------------------------------------------------------------
# Test du code:
if __name__=="__main__":
    # Dataframe de test
    exam_data  = {'nom': ['Anne', 'Alex', 'catherine', 'Jean', 'Emillie', 
                            'Michel', 'Matieu', 'Laura', 'Kevin', 'Jonas'],
        'note': [12.5, 9, 16.5, np.nan, 9, 20, 14.5, np.nan, 8, 19],
        'qualification': ['yes', 'no', 'yes', 'no', 'no', 'yes', 'yes', 
                            'no', 'no', 'yes']}
    labels = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']
    df = pd.DataFrame(exam_data, index=labels)
    
    # Teste de multiples format: fonction convertisseur
    format_in = ["json", "xml", "csv", "xlsx", "sql"]
    format_out = ["json", "xml", "csv", "xlsx", "sql"]
    print(f"{' Début du Script: ':#^78}")
    start = time.time()
    
    # Test multi-format
    for ext in format_in[:-1]:
        chemin = convert(f"DATA/DATA_TEST/{ext}/output.{ext}", format_out[:-1], labels={"nom": "name","note": "notes","qualification": "certification"})
        print("Voici les chemins d'accès aux fichiers: \n", chemin)
        print()
    
    # Test en unitaire
    # convert("DATA/test.sql", "xml")
    # convert("DATA/test.sql", "json")
    # convert("DATA/test.sql", "csv")
    # convert("DATA/test.sql", "xlsx")

    # convert("DATA/test.xml", "sql")
    convert("DATA/test.xml", "json")
    convert("DATA/test.xml", "csv")
    convert("DATA/test.xml", "xlsx")

    # convert("DATA/test.json", "sql")
    convert("DATA/test.json", "xml")
    convert("DATA/test.json", "csv")
    convert("DATA/test.json", "xlsx")

    # convert("DATA/test.csv", "sql")
    convert("DATA/test.csv", "xml")
    convert("DATA/test.csv", "json")
    convert("DATA/test.csv", "xlsx")

    # convert("DATA/test.xlsx", "sql")
    convert("DATA/test.xlsx", "xml")
    convert("DATA/test.xlsx", "json")
    convert("DATA/test.xlsx", "csv")
    
    end = time.time()
    elapsed = end - start
    print()
    print(f'Temps d\'exécution: {elapsed:5.2f}ms')
    print(f"{' Fin du Script: ':#^78}")

#------------------------------------------------------------------------------