Fonction unique:
 1. Path "str": Prends en entré le fichier (chemin)
 2. Format "str | []" : Prends un argument qui permet de retourne autant de fichier que de format pour le type de fichier de sorti
 3. Techno-Pandas : prends un format pivot pour être converti dans tous les autres
 4. Split "int": Prends un argument qui permet de choisir le nombre de fichiers (découpe les données en différents fichiers de même taille suivants les LIGNES)
 5. Labels "dict" : Prends un argument qui permet de changer les noms de colonnes
 6. Filtre "str" : Prends un argument qui permet de trier le document avant export
 7. Return "[]": Export le ou les fichiers converties sur le même chemin, puis retourne la liste des chemins

Détails arguments:
 1. Path "str": "/DATA/file.extension"
 2. Type "str | []": ["json", "xml", "csv", "excel", "sql"]
 3. Techno-Pandas : Dataframe
 4. Split "int": df.shape ==> (891, 12)
 Algo:
 - df.shape[0] / split: int = nb_lign_by_part
 Si split est impair:
 - nb_by_part * (split - 1) = nb_lign_for_n_part
 - df.shape[0] - nb_lign_for_n_part = nb_lign_for_last_part
 5. Labels "dict" :  {"ancien nom de colonne": "nouveau nom de colonne"}
 6. Filtre "str" : ["sort", "xml", "csv", "excel", "sql"]
 7. Return "[]": PASS
